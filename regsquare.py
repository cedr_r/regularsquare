from cv2 import resize
import fontforge
from PIL import Image, ImageOps 
import csv, glob, os
import sys
import numpy as np
import svgwrite

gly_Export = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","one","two","three","four","five","six","seven","eight","nine","zero","cedilla","grave","plus","less","equal","greater","comma","period","slash","colon","semicolon","question","at","backslash","ampersand","percent","quotesingle","asterisk","exclam","hyphen","underscore","parenleft","bracketleft","braceleft","parenright","bracketright","braceright","ae","AE","grave","acute","dieresis","cedilla","Agrave","Acircumflex","Adieresis","Aacute","agrave","acircumflex","adieresis","aacute","Egrave","Ecircumflex","Edieresis","Eacute","egrave","ecircumflex","edieresis","eacute","Igrave","Icircumflex","Idieresis","Iacute","igrave","icircumflex","idieresis","iacute","Ograve","Ocircumflex","Odieresis","Oacute","ograve","ocircumflex","odieresis","oacute","Ugrave","Ucircumflex","Udieresis","Uacute","ugrave","ucircumflex","udieresis","uacute"]
FontTransform = sys.argv[1]
FuturFont = sys.argv[2]


def export_bitmap(font_file, size):
    font = fontforge.open(font_file)
    for gly in font.glyphs():
        if gly.glyphname in gly_Export :
            gly.export("bitmap/" + gly.glyphname + ".bmp", size)
        print(gly.glyphname)
    print(font)

def resize_bitmap(bitmap_file):
     cmd =  os.system("convert " + bitmap_file + " -gravity center -extent 1500x1000 " + bitmap_file)

def grab_density(bitmap_file,fontName):
    basename = os.path.basename(bitmap_file).replace('.bmp', '')
    stepW = 100
    stepH = 100
    im = Image.open(bitmap_file, 'r')
    stepNumberH = int((im.height)/100)
    stepNumberW = int((im.width)/100)
    listPP = im.load()
    densityBox = 0
    z = 0
    u = 0

    if not os.path.exists('csv/'+ fontName + '/'):
            os.makedirs('csv/'+ fontName +'/')

    with open( 'csv/'+ fontName +'/'+basename+".csv", 'w') as f:
        writer = csv.writer(f)
        Lts = []
        while z < stepNumberH:
            L = [] 
            u = 0
            while u < stepNumberW:
                for x in range(0,stepW):
                    for y in range(0,stepH):
                        if listPP[x+(u*stepW),y+(z*stepH)] == (255,255,255):
                            densityBox = densityBox
                        else:
                            densityBox += 1
                L.append(int(densityBox/100))
                densityBox = 0
                u += 1 
            Lts.append(L)
            z += 1
        writer.writerows(Lts)
        print (basename)


def csv_to_svg(csv_file, fontName):
    
    basename = os.path.basename(csv_file).replace('.csv', '')
    with open(csv_file, 'r') as file:
        reader = csv.reader(file)
        list_csv = []
        for row in reader: 
            list_csv.append(row)
        width = len(list_csv[0])
        height = len(list_csv)
        

        dwg = svgwrite.Drawing('test.svg', size=(width, height))
        dwg.viewbox(0, 0, width*100, height*100)
        y = 0
        for line in list_csv:
            x = 0
            for val in line: 
                #print(val)
                #print('---------')
                x += 100
                if val != '0':
                    part = dwg.rect(insert=((x+int(val)/4), (y+int(val)/4)), size=((100-int(val)/2), (100-int(val)/2)), rx=None, ry=None, stroke="black", fill="none", stroke_width=str(int(val)/2))
    
                    dwg.add(part)

            y += 100
            # if val[0] == 255:
            #     part = dwg.ellipse(center=(val[1] + 1, val[2] + 0.5), r=(.5, .5), fill='black')
    
        
        if not os.path.exists('svg/'+ fontName + '/'):
            os.makedirs('svg/'+ fontName +'/')

        dwg.saveas('svg/'+ fontName +'/'+basename+'.svg')
   

def export_csv(files,fontName):
    for bitmap in glob.glob(files):
        grab_density(bitmap,fontName)

def sizeBit(files):
    for bitmap in glob.glob(files):
        resize_bitmap(bitmap)

def export_csv_to_svg(files,fontName):
    for csv in glob.glob(files):
        csv_to_svg(csv,fontName)



fontExport= FuturFont

export_bitmap('input/'+ FontTransform, 1000)
sizeBit('bitmap/*.bmp')
export_csv('bitmap/*.bmp', fontExport)
export_csv_to_svg("csv/" + fontExport + "/*.csv", fontExport)

cmd =  os.system("bash generate.sh "+ fontExport)

