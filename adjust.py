#/usr/bin/env python2

import fontforge
import glob
import sys
import os
import lxml.etree as ET
import psMat

FontName = sys.argv[1]
yTranslate = -204

font = fontforge.open('FINAL/'+ FontName +'/'+ FontName +'.sfd')

trs = psMat.translate(0, int(yTranslate)) 
font.selection.all()
font.transform(trs)

font.generate('FINAL/' + FontName + '/' + FontName + '.sfd')
font.generate('FINAL/' + FontName + '/' + FontName + '.otf')